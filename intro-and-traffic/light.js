function light(n) {
  var pic = document.getElementById("bulb");
  var box = document.getElementById("txt");
  var body = document.getElementById("body");
  if (n === 1) {
    pic.src = "on.gif";
    box.innerHTML = "Button is down, light on";
    body.style.backgroundColor = "#FFFFCC";
  }
  if (n === 0) {
    pic.src = "off.gif";
    box.innerHTML = "Button is up, light off";
    body.style.backgroundColor = "#AAAAAA";
  }
}

/* Change between green, yellow, and red */
function changeLight() {
  var lightBox = document.getElementById("lightBox");
  if (lightBox.style.backgroundColor === "green") {
    lightBox.style.backgroundColor = "yellow";
    lightBox.innerHTML = "yellow";
  } else if (lightBox.style.backgroundColor === "yellow") {
    lightBox.style.backgroundColor = "red";
    lightBox.innerHTML = "red";
  } else if (lightBox.style.backgroundColor === "red") {
    lightBox.style.backgroundColor = "green";
    lightBox.innerHTML = "green";
  }
}

/* Traffic light */
function changeLight2() {
  var green = document.getElementById("green");
  var yellow = document.getElementById("yellow");
  var red = document.getElementById("red");
  if (green.style.backgroundColor === "green") {
    green.style.backgroundColor = "black";
    yellow.style.backgroundColor = "yellow";
  } else if (yellow.style.backgroundColor === "yellow") {
    yellow.style.backgroundColor = "black";
    red.style.backgroundColor = "red";
  } else {
    red.style.backgroundColor = "black";
    green.style.backgroundColor = "green";
  }
}

document.getElementById("change").addEventListener("click", changeLight2);

var randSec = Math.floor(Math.random() * 5) + 5; // 5-10 seconds
console.log(randSec);
setInterval(function() {
  alert("Traffic Jam");
  document.getElementById("change").removeEventListener("click", changeLight2);
}, randSec * 1000);
