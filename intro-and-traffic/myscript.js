var myName = "Jeffrey";
var myName = 34;

let number1 = 12;
let number2 = "12";

function myFunction(name = "Jeffrey") {
  /*
    if (true) {
        let myFunctionVariable = "Hi from function";
    }
    console.log(myFunctionVariable);*/
  console.log(name);
}

myOtherFunction = myFunction;
//myOtherFunction("Fred");

//myFunction();

class Person {
  constructor(name, age) {
    this._name = name;
    this._age = age;
  }

  get name() {
    return this._name;
  }

  set name(newName) {
    if (newName) {
      this._name = newName;
    }
  }

  get age() {
    return this._age;
  }

  set age(newAge) {
    if (newAge) {
      this._age = newAge;
    }
  }

  incrementAge() {
    this._age++;
  }
}

var me = new Person("Jeffrey", 21);
console.log(me.name);
console.log(me.age);
me.incrementAge();
console.log(me.age);
me.age = -1;
console.log(me._age);

me.student = false;
console.log(me.student);

myArray = [
  new Person("Jeffey", 21),
  new Person("Fanng", 100),
  new Person("Vince", 9001)
];

myArray.forEach(person => console.log(person));
