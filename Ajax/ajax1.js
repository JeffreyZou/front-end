var oAjax;

document.getElementById("mmm").addEventListener("click", make_request);

function make_request() {
  oAjax = new XMLHttpRequest();
  // Function is called after async
  oAjax.onreadystatechange = function() {
    show_results();
  };
  oAjax.open("GET", "hello.txt", true); // true for async
  oAjax.send(null); // No body for GET
}

function show_results() {
  console.log(oAjax.readyState);
  console.log(oAjax.status);
  if (oAjax.readyState === 4 && oAjax.status === 200) {
    document.getElementById("div1").innerHTML = oAjax.responseText;
  }
}
